const section = document.createElement('section');
section.innerHTML = `
    
    <h3 class="join-us--title" >Join Our Program</h3>
    <p class="desc">Sed do eiusmod tempor incididunt 
                ut labore et dolore magna aliqua.</p>
    <form class="form">
    <input class="input" placeholder="Email" />
    <button class="btn" type="submit">SUBSCRIBE</button>
    </form>

`;
document.querySelector('footer').before(section);
section.classList.add('section');
document.querySelector('.form').addEventListener('submit', (e) => {
  e.preventDefault();
  const inputValue = document.querySelector('.input')?.value;
  console.log(inputValue);
});
